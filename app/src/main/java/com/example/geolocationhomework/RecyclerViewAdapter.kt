package com.example.geolocationhomework

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlin.math.round

//Кастомный класс адаптера для recyclerView
class RecyclerViewAdapter: RecyclerView.Adapter<RecyclerViewAdapter.VH>() {

    private lateinit var list: List<FitnessModel>

    //Принимаем значения
    open fun setData (list: List<FitnessModel>) {
        this.list = list
    }

    //Создаём новый объект класса VH и передаём view'шку item'а
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.some_item, parent, false))
    }

    //Запрашиваем кол-во элементов в recyclerView
    override fun getItemCount(): Int {
        return list.size
    }

    //Задаём тексты в item'е
    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.dateandyear.text = "${list.get(position).date} ${list.get(position).year}"
        holder.distance.text = "- ${(list.get(position).dist / 1000).toInt()} км"
    }

    //Кастомный класс viewHolder'а для recyclerView
    open class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //Задаём айдишки
        val dateandyear = itemView.findViewById<TextView>(R.id.yearanddate)
        val distance = itemView.findViewById<TextView>(R.id.distance)
    }
}