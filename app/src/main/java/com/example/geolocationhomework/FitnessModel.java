package com.example.geolocationhomework;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;

//Класс описания "записи"
@Table(database = MyDatabase.class, name = "newFitTable")
public class FitnessModel {

    @PrimaryKey
    @Column
    int _id;

    @Column
    String date;

    @Column
    String year;

    @Column
    int dist;

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        this._id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getDist() {
        return dist;
    }

    public void setDist(int dist) {
        this.dist = dist;
    }
}
