package com.example.geolocationhomework;

import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

//Класс "описания" приложения
public class MyApplication extends Application {

    //При создании приложения
    @Override
    public void onCreate() {
        super.onCreate();
        //Инициализируем manager для таблицы
        FlowManager.init(new FlowConfig.Builder(this).build());
    }
}
