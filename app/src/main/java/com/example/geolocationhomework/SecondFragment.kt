package com.example.geolocationhomework

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.raizlabs.android.dbflow.sql.language.SQLite
import kotlinx.android.synthetic.main.second_fragment.*
import java.util.*

//Класс второго фрагмента
class SecondFragment: Fragment() {

    lateinit var root: View
    lateinit var fitnessList2: List<FitnessModel>

    //При создании фрагмента
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //Находим view нашего фрагмента
        root = inflater.inflate(R.layout.second_fragment, container, false)

        //Обновляем значения таблицы
        var fitnessList = SQLite.select().from(FitnessModel::class.java).queryList()

        //Создаём и настраиваем значения адаптера
        val adapter = RecyclerViewAdapter()
        adapter.setData(fitnessList)

        //Объявляем recyclerView и присваиваем адаптер
        val recyclerView1 = root.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView1.adapter = adapter

        val timer = Timer()

        timer?.schedule(object : TimerTask() {
            override fun run() {
                //Обновляем значения таблицы
                fitnessList2 = SQLite.select().from(FitnessModel::class.java).queryList()
                if (fitnessList.size != fitnessList2.size) {
                    fitnessList = fitnessList2
                    //Создаём и настраиваем значения адаптера
                    val adapter = RecyclerViewAdapter()
                    adapter.setData(fitnessList2)

                    //Объявляем recyclerView и присваиваем адаптер
                    activity!!.runOnUiThread{
                        recyclerView1.adapter = adapter
                    }
                }
            }
        }, 0, 200)

        return root
    }

//    override fun onResume() {
//        super.onResume()
//        //Обновляем значения таблицы
//        val fitnessList = SQLite.select().from(FitnessModel::class.java).queryList()
//
//        //Создаём и настраиваем значения адаптера
//        val adapter = RecyclerViewAdapter()
//        adapter.setData(fitnessList)
//
//        //Объявляем recyclerView и присваиваем адаптер
//        val recyclerView1 = root.findViewById<RecyclerView>(R.id.recyclerView)
//        recyclerView1.adapter = adapter
//    }
}