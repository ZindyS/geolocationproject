package com.example.geolocationhomework

import android.location.Location
import android.location.LocationListener
import android.os.Bundle

//Абстрактный кастомный класс с самыми важными и частоиспользуемыми методами класса LocationListener
abstract class SimpleLocationListener : LocationListener{
    //При изменении локации
    override fun onLocationChanged(location: Location) {

    }

    //При изменении статуса
    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

    }

    //Тут я хз
    override fun onProviderEnabled(provider: String?) {

    }

    //Тут я хз
    override fun onProviderDisabled(provider: String?) {

    }
}