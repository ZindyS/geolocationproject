package com.example.geolocationhomework

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.view.ViewParent
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabItem
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_main.*

//Класс начального активити
class MainActivity : AppCompatActivity() {

    //При создании активити
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Задаём айдишки
        val tabs = findViewById<TabLayout>(R.id.tabLayout)
        val viewPager = findViewById<ViewPager>(R.id.viewPager)

        //Настраиваем адаптер для viewPager'а
        setupViewPager(viewPager!!)

        //Настраиваем tabLayout
        tabs!!.setupWithViewPager(viewPager)
    }

    //Функция настройки адаптера для viewPager
    private fun setupViewPager(viewPager: ViewPager) {
        //Создаём объект кастомного класса
        val adapter = ViewPagerAdapter(supportFragmentManager)

        //Добавляем фрагменты
        adapter.addFragment(FirstFragment(), "Запись")
        adapter.addFragment(SecondFragment(), "История")

        //Присваиваем адаптер
        viewPager.adapter = adapter
    }

    //Кастомный класс адаптера для viewPager'а
    //Взял отсюда: https://stackoverflow.com/questions/61472980/use-tablayout-with-fragments-and-viewpager-kotlin
    internal inner class ViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        //Получаем элемент
        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        //Получаем кол-во элементов
        override fun getCount(): Int {
            return mFragmentList.size
        }

        //Добавляем фрагмент
        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        //Получаем "заголовки" фрагментов
        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }
}