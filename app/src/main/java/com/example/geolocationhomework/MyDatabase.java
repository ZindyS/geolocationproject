package com.example.geolocationhomework;

import com.raizlabs.android.dbflow.annotation.Database;

//Класс описания базы данных
@Database(name = MyDatabase.NAME, version = MyDatabase.VERSION)
public class MyDatabase {
    public static final String NAME =  "FitnessDatabase";
    public static final int VERSION = 1;
}
