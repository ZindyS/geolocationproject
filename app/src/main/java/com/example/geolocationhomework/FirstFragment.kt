package com.example.geolocationhomework

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.raizlabs.android.dbflow.kotlinextensions.delete
import com.raizlabs.android.dbflow.kotlinextensions.save
import com.raizlabs.android.dbflow.sql.language.SQLite
import kotlinx.android.synthetic.main.first_fragment.*
import java.text.SimpleDateFormat
import java.util.*

//Класс первого фрагмента
class FirstFragment: Fragment() {

    private lateinit var lm: LocationManager
    private val model = FitnessModel()
    private lateinit var startLocation: Location
    private lateinit var organizationList: List<FitnessModel>
    private lateinit var dist: TextView
    private var biba = false
    private var biba2 = false
    private var isNotStopped = false

    //Создаём объект, основанный на абстрактном классе SimpleLocationListener
    private val locationCallback = object : SimpleLocationListener() {
        //При изменении локации
        override fun onLocationChanged(location: Location) {
            super.onLocationChanged(location)

            //Если прошлая локация не была иниацилизирована или перед этим была нажата кнопка "Стоп"
            if(!::startLocation.isInitialized || biba2) {
                startLocation = location
                biba2 = false
            }

            val double = floatArrayOf(0f)

            //Определяем расстояние между текущей локацией и прошлой
            Location.distanceBetween(
                startLocation.latitude,
                startLocation.longitude,
                location.latitude,
                location.longitude,
                double
            )

            //Проверяем дату и год
            val sdf = SimpleDateFormat("dd/MM")
            val currentDate = sdf.format(Date())
            val year = SimpleDateFormat("yyyy")
            val currentYear = year.format(Date())
            var bruh = true


            if (isNotStopped) {
                model.dist = (double[0] + model.dist).toInt()
            } else {
                model._id = organizationList.size
                model.date = currentDate
                model.dist = double[0].toInt()
                model.year = currentYear
                isNotStopped = true
            }

            //Обновляем значения таблицы
            organizationList = SQLite.select().from(FitnessModel::class.java).queryList()

            //Обновляем текст
            dist.text = "Пройдено ${(model.dist).toInt()} метров"

            //Присваиваем стартовой локации значение текущей (для дальнейшего просчитывания расстояния)
            startLocation = location
        }
    }

    //При создании фрагмента
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //Находим view нашего фрагмента
        val root = inflater.inflate(R.layout.first_fragment, container, false)

        //Присваиваем айдишки
        val start1 = root.findViewById<Button>(R.id.start)
        val stop1 = root.findViewById<Button>(R.id.stop)
        dist = root.findViewById(R.id.dist)

        //Сразу проверяем разрешения (просто мне так больше нравится)
        if (!isUHavePerm()) {
            givePerm()
        }

        //Показываем пользователю, что сейчас запись не идёт
        stop1.setBackgroundResource(R.drawable.cornerschoosed)

        //При нажатии на "Старт"
        start1.setOnClickListener {
            if (!biba) { //Если кнопка не была нажата до этого
                //Проверка на разрешения
                if (isUHavePerm()) {
                    //Принимаем локацию
                    getLocation()
                } else {
                    //Запрашиваем разрешение
                    givePerm()
                }
                biba = true
            } else { //Если кнопка была нажата до этого
                Toast.makeText(context, "Запись уже начата!", Toast.LENGTH_SHORT).show()
            }
            //Показываем пользователю, что запись начата
            start1.setBackgroundResource(R.drawable.cornerschoosed)
            stop1.setBackgroundResource(R.drawable.corners)
        }

        //При нажатии на "Стоп"
        stop1.setOnClickListener {
            //Если locationManager был инициализирован, то убираем у него
            if (::lm.isInitialized) {
                lm.removeUpdates(locationCallback)
            }
            biba = false
            biba2 = true
            isNotStopped = false

            model.save()

            dist.text = "Пройдено 0 метров"

            //Показываем пользователю, что запись остановлена
            stop1.setBackgroundResource(R.drawable.cornerschoosed)
            start1.setBackgroundResource(R.drawable.corners)
        }

        //Обновляем значения таблицы
        organizationList = SQLite.select().from(FitnessModel::class.java).queryList()

        dist.text = "Пройдено 0 метров"

        return root
    }

    //При "уничтожении" приложения
    override fun onDestroy() {
        super.onDestroy()

        //Удаляем слушатель, если locationManager инициализирован
        if (::lm.isInitialized) {
            lm.removeUpdates(locationCallback)
        }
    }

    //Функция проверки разрешений
    private fun isUHavePerm(): Boolean {
        return ContextCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    //Функция предоставления разрешений
    private fun givePerm() {
        ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 0)
    }

    //Обработка ответа пользователя на предоставление разрешения
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == 0) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) { //Если предоставил разрешение, то получаем локацию
                getLocation()
            } else { //Если не предоставил разрешение, то показываем AlertDialog
                val builder = AlertDialog.Builder(context!!)
                builder.setTitle("Предоставьте разрешение")
                    .setNeutralButton("Предоставить", DialogInterface.OnClickListener { dialog, which ->
                        givePerm()
                    })
                    .show()
            }
        }
    }

    //Функция получения локации
    private fun getLocation() {
        //Создаём объект класса LocationManager
        lm = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        //Проверяем, включена ли геолокация по GPS'у
        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) { //Если да - подписываемся на обновления
            requestLocationUpdates()
        } else { //Если нет - запрашиваем
            val builder = AlertDialog.Builder(context!!)
            builder.setTitle("Включите геолокацию")
                .setNeutralButton("Включить", DialogInterface.OnClickListener { dialog, which ->
                    val settingsIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivity(settingsIntent)
                })
                .show()
        }
    }

    //Функция подписки на обновления локации
    @SuppressLint("MissingPermission")
    private fun requestLocationUpdates() {
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10f, locationCallback)
    }
}